from fastapi import FastAPI, HTTPException, Request
from fastapi.middleware.cors import CORSMiddleware
from openai import OpenAI
import httpx
import os
import bson
import random
import json
from dotenv import load_dotenv
from pymongo import MongoClient
from datetime import datetime, timedelta
import jwt
import time
import base64
import json

load_dotenv()

app = FastAPI()

appVersion = "1.1.0"
json_version = f'{{"serverUp": true, "version": "{appVersion}"}}'
json_version_Loaded = json.loads(json_version)

Open_AI_Token = os.getenv('Open_AI_Token')
Spotify_Client_ID = os.getenv('Spotify_Client_ID')
Spotify_Client_Secret = os.getenv('Spotify_Client_Secret')
mongodb_ConnectionString = os.getenv('Mongo_DB_String')
Apple_Key_ID = os.getenv('Apple_Key_ID')
Apple_Issuer_ID = os.getenv('Apple_Issuer_ID')
Apple_Private_Key = os.getenv('Apple_Private_Key')

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

async def get_access_token(client_id, client_secret):
    url = "https://accounts.spotify.com/api/token"
    headers = {
        "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
    }
    data = {
        "grant_type": "client_credentials",
        "client_id": client_id,
        "client_secret": client_secret
    }

    async with httpx.AsyncClient() as client:
        response = await client.post(url, headers=headers, data=data)

    if response.status_code == 200:
        token_data = response.json()
        access_token = token_data.get("access_token")
        return access_token
    else:
        raise HTTPException(status_code=500, detail="Failed to retrieve Spotify token")

async def get_playlist_data(access_token, playlist_id):
    url = f'https://api.spotify.com/v1/playlists/{playlist_id}?fields=tracks.items(track(name,artists(name),album(name)))'
    headers = {
        'Authorization': f'Bearer {access_token}'
    }

    async with httpx.AsyncClient() as client:
        response = await client.get(url, headers=headers)

    if response.status_code == 200 : 
        playlist_data = response.json()
        return playlist_data
    else:
        raise HTTPException(status_code=500, detail="Failed to retrieve Spotify playlist data")

async def get_track_data(access_token, track_name, artist_name):
    url = f'https://api.spotify.com/v1/search?q=track:{track_name} artist:{artist_name}&type=track'
    headers = {
        'Authorization': f'Bearer {access_token}'
    }

    async with httpx.AsyncClient() as client:
        response = await client.get(url, headers=headers)

    if response.status_code == 200:
        track_data = response.json()
        return track_data
    else:
        raise HTTPException(status_code=500, detail="Failed to retrieve Spotify track data")

def parse_value(text):
    try:
        #print(text)
        json_start = text.find("{")
        json_end = text.rfind("}")
        if json_start == -1 or json_end == -1:
            raise ValueError("JSON structure not found")

        json_string = text[json_start:json_end + 1]

        # Remove escape characters
        json_string = json_string.replace("“", '"').replace("”", '"').replace("‘", "'").replace("’", "'")
        json_string = json_string.replace("\\'", "'").replace('\\"', '"').replace("\\n", "").replace("\\t", "")

        # Correctly handle nested JSON
        brace_count = 0
        for i, char in enumerate(json_string):
            if char == '{':
                brace_count += 1
            elif char == '}':
                brace_count -= 1
                if brace_count == 0:
                    json_string = json_string[:i + 1]
                    break
        
        #print(json_string)
        json_data = json.loads(json_string)
        return json_data
    except json.JSONDecodeError as e:
        raise HTTPException(status_code=500, detail=f"JSON parsing error: {e}")

def verify_file(file_paths):
    for path in file_paths:
        if not os.path.isfile(path):
            raise FileNotFoundError(f"File {path} not found.")
        if not os.access(path, os.R_OK):
            raise PermissionError(f"File {path} cannot be read.")

def getUserCoins(userUUID: str) -> int:
    if userExists(userUUID):
        try:
            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
        
            item_details = EchoInsightDB.find_one({"userUUID": userUUID})
            
            item_details_json = bson.json_util.dumps(item_details)
            item_details = json.loads(item_details_json)
        
            coins = item_details.get("coins")
            return coins
        except:
            return 0
    else:
        return 0

def removeUserCoin(userUUID: str) -> bool:
    if userExists(userUUID):
        try:
            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
        
            item_details = EchoInsightDB.find_one({"userUUID": userUUID})
            
            item_details_json = bson.json_util.dumps(item_details)
            item_details = json.loads(item_details_json)
        
            coins = item_details.get("coins")
        
            filter = {'userUUID': userUUID}
            newvalues = {"$set": {'coins': coins - 1}}
            EchoInsightDB.update_one(filter, newvalues)
            return True
        except:
            return False
    else:
        return False

def addUsedUserCoin(userUUID: str) -> bool:
    if userExists(userUUID):
        try:
            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
            
            item_details = EchoInsightDB.find_one({"userUUID": userUUID})
                
            item_details_json = bson.json_util.dumps(item_details)
            item_details = json.loads(item_details_json)
            
            coins = item_details.get("stats").get("usedCoins")
            
            filter = {'userUUID': userUUID}
            newvalues = {"$set": {'stats.usedCoins': coins + 1}}
            EchoInsightDB.update_one(filter, newvalues)
            return True
        except:
            return False
    else:
        return False

def addRefundedUserCoin(userUUID: str) -> bool:
    if userExists(userUUID):
        try:
            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
            
            item_details = EchoInsightDB.find_one({"userUUID": userUUID})
                
            item_details_json = bson.json_util.dumps(item_details)
            item_details = json.loads(item_details_json)
            
            coins = item_details.get("stats").get("refundedCoins")
            
            filter = {'userUUID': userUUID}
            newvalues = {"$set": {'stats.refundedCoins': coins + 1}}
            EchoInsightDB.update_one(filter, newvalues)
            return True
        except:
            return False
    else:
        return False

def addRateLimitedTimes(userUUID: str) -> bool:
    if userExists(userUUID):
        try:
            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
            
            item_details = EchoInsightDB.find_one({"userUUID": userUUID})
                
            item_details_json = bson.json_util.dumps(item_details)
            item_details = json.loads(item_details_json)
            
            rate = item_details.get("stats").get("rateLimitedTimes")
            
            filter = {'userUUID': userUUID}
            newvalues = {"$set": {'stats.rateLimitedTimes': rate + 1}}
            EchoInsightDB.update_one(filter, newvalues)
            return True
        except:
            return False
    else:
        return False

def addUserCoins(userUUID: str, addedCoins: int) -> bool:
    if userExists(userUUID):
        try:
            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
            
            item_details = EchoInsightDB.find_one({"userUUID": userUUID})
                
            item_details_json = bson.json_util.dumps(item_details)
            item_details = json.loads(item_details_json)
            
            coins = item_details.get("coins")
            totalPurchasedCoins = item_details.get("stats").get("purchasedCoins")
            print(coins)
            print(totalPurchasedCoins)

            filter = {'userUUID': userUUID}
            newvalueAdded = {"$set": {
                'coins': coins + addedCoins,
                'stats.purchasedCoins': totalPurchasedCoins + addedCoins
            }}
            EchoInsightDB.update_one(filter, newvalueAdded)
            return True
        except:
            return False
    else:
        return False

def addSuccessfullCoinsUserCoin(userUUID: str) -> bool:
    if userExists(userUUID):
        try:
            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
            
            item_details = EchoInsightDB.find_one({"userUUID": userUUID})
                
            item_details_json = bson.json_util.dumps(item_details)
            item_details = json.loads(item_details_json)
            
            coins = item_details.get("stats").get("successfullCoins")
            
            filter = {'userUUID': userUUID}
            newvalues = {"$set": {'stats.successfullCoins': coins + 1}}
            EchoInsightDB.update_one(filter, newvalues)
            return True
        except:
            return False
    else:
        return False

def addErrorCoinsUserCoin(userUUID: str) -> bool:
    if userExists(userUUID):
        try:
            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
            
            item_details = EchoInsightDB.find_one({"userUUID": userUUID})
                
            item_details_json = bson.json_util.dumps(item_details)
            item_details = json.loads(item_details_json)
            
            coins = item_details.get("stats").get("errorCoins")
            
            filter = {'userUUID': userUUID}
            newvalues = {"$set": {'stats.errorCoins': coins + 1}}
            EchoInsightDB.update_one(filter, newvalues)
            return True
        except:
            return False
    else:
        return False

def userExists(userUUID: str) -> bool:
    try:
        CONNECTION_STRING = mongodb_ConnectionString
        client = MongoClient(CONNECTION_STRING)
        dbname = client.EchoInsightDB
        EchoInsightDB = dbname["Users"]
        item_details = EchoInsightDB.find_one({"userUUID": f"{userUUID}"})
        if item_details:
            userExistence = True
        else:
            userExistence = False
        return userExistence
    except:
        return False

def addSongUser(userUUID: str, imageURL: str, songName: str, artistName: str, urlSong: str, musicService: str, playlistID: str) -> bool:
    if userExists(userUUID):
        try:
            utc_now = datetime.utcnow()
            central_time_offset = timedelta(hours=-6)
            current_datetime = utc_now + central_time_offset

            client = MongoClient(mongodb_ConnectionString)
            dbname = client.EchoInsightDB
            EchoInsightDB = dbname["Users"]
            
            filter = {'userUUID': userUUID}
            new_song = {
                "name": songName,
                "image": imageURL,
                "artist": artistName,
                "url": urlSong,
                "userPlaylistIdUsed": playlistID,
                "dateGenerated": current_datetime,
                "usedService": musicService
            }
            update = {
                "$push": {
                    "songs": new_song
                }
            }
            
            EchoInsightDB.update_one(filter, update)
            return True
        except:
            return False
    else:
        return False

def generate_jwt_token():
    headers = {
        'alg': 'ES256',
        'kid': Apple_Key_ID
    }

    payload = {
        'iss': Apple_Issuer_ID,
        'iat': int(time.time()),
        'exp': int(time.time()) + 3600,
        'aud': 'appstoreconnect-v1',
        'bid': 'com.AlfredTechDev.EchoInsight'
    }

    token = jwt.encode(payload, Apple_Private_Key, algorithm='ES256', headers=headers)
    
    return token

def decode_jwt(jwt):
    try:
        header, payload, signature = jwt.split('.')
        
        payload += '=' * (-len(payload) % 4)

        decoded_payload = base64.urlsafe_b64decode(payload)

        payload_data = json.loads(decoded_payload)

        product_id = payload_data.get('productId')
        
        return product_id
    
    except Exception as e:
        print(f"Error decoding JWT: {e}")
        return None


@app.get("/recommendMusicDev/")
async def recommend_music(playlistID: str, openAIKey: str, spotifyClientID: str, spotifyClientSecret: str):
    playlist_id = playlistID

    api_key = openAIKey
    client = OpenAI(api_key=api_key)
    client_id = spotifyClientID
    client_secret = spotifyClientSecret

    access_token = await get_access_token(client_id, client_secret)
    playlist_songs = []

    if access_token:
        playlist_data = await get_playlist_data(access_token, playlist_id)
        if playlist_data:
            songs = playlist_data["tracks"]["items"]
            random.shuffle(songs)
            playlist_songs = songs[:25]
            playlist_songs = json.dumps(playlist_songs).replace('"', '\\"')

    assistant = client.beta.assistants.create(
        name="Music Suggestions",
        instructions='You are an expert music analyst. Use your knowledge base to answer questions about which song fits more for me. Also just answer only with the song name and song artists for each song you recommend, you should not write anything else just the songs in JSON format on the answer, and you should not use ` symbol, json word nor the new line symbol on the answer. Example of how the correct answer should be: {"songs":[ {"name":"Song Name", "Artists":[{"Artist1":"Artist Name"},{"Artist2":"Artist Name"}]}, {"name":"Song Name", "Artists":[{"Artist1":"Artist Name"},{"Artist2":"Artist Name"}]}, {"name":"Song Name", "Artists":[{"Artist1":"Artist Name"},{"Artist2":"Artist Name"}]}, {"name":"Song Name", "Artists":[{"Artist1":"Artist Name"},{"Artist2":"Artist Name"}]}, {"name":"Song Name", "Artists":[{"Artist1":"Artist Name"},{"Artist2":"Artist Name"}]} ]} REMEMBER THE ANSWER SHOULD ONLY BE A JSON LIKE ON THE EXAMPLE',
        model="gpt-4o",
        tools=[{"type": "file_search"}],
    )

    file_paths = [
        "datasets/dataset1.json",
        "datasets/dataset2.json",
        "datasets/dataset3.json",
        "datasets/dataset4.json",
        "datasets/dataset5.json",
        "datasets/dataset6.json"
    ]

    try:
        verify_file(file_paths)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"File verification failed: {e}")

    uploaded_files = []
    for file_path in file_paths:
        try:
            message_file = client.files.create(
                file=open(file_path, "rb"), purpose="assistants"
            )
            uploaded_files.append(message_file.id)
        except Exception as e:
            raise HTTPException(status_code=500, detail=f"Failed to upload file {file_path}: {e}")

    if not uploaded_files:
        raise HTTPException(status_code=500, detail="No files were uploaded successfully.")

    try:
        thread = client.beta.threads.create(
            messages=[
                {
                    "role": "user",
                    "content": f"Search for the following songs in the database: \n{playlist_songs} For each matched song, look for its “popularity”, “duration_ms”, “explicit”, “danceability”, “energy”, “key”, “loudness”, “mode”, “speechiness”, “acousticness”, “instrumentalness”, “liveness”, “valence”, “tempo”, “time_signature”, and “track_genre” details. Based on these songs and their attributes, identify 5 similar songs from the database that I may like and provide only the name an artist of each song.",
                    "attachments": [{"file_id": file_id, "tools": [{"type": "file_search"}]} for file_id in uploaded_files]
                }
            ]
        )
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to create thread: {e}")

    try:
        run = client.beta.threads.runs.create_and_poll(
            thread_id=thread.id, assistant_id=assistant.id
        )

        messages = list(client.beta.threads.messages.list(thread_id=thread.id, run_id=run.id))
        parsed_content = parse_value(f"{messages}")
        return parsed_content
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to create and poll run: {e}")

@app.get("/EchoInsightMusicRecommendSpotify/")
async def recommend_music(PlaylistID: str, userUUID: str):
    if getUserCoins(userUUID) > 0:
        playlist_id = PlaylistID

        api_key = Open_AI_Token
        client = OpenAI(api_key=Open_AI_Token)
        client_id = Spotify_Client_ID
        client_secret = Spotify_Client_Secret

        access_token = await get_access_token(client_id, client_secret)
        playlist_songs = []

        if access_token:
            playlist_data = await get_playlist_data(access_token, playlist_id)
            if playlist_data:
                songs = playlist_data["tracks"]["items"]
                random.shuffle(songs)
                # playlist_songs = songs[:25]
                playlist_songs = songs[:50]
                playlist_songs = json.dumps(playlist_songs).replace('"', '\\"')

        removeUserCoin(userUUID)
        addUsedUserCoin(userUUID)

        try:
            thread = client.beta.threads.create(
                messages=[
                    {
                        "role": "user",
                        "content": playlist_songs,
                        "attachments": []
                    }
                ]
            )
        except Exception as e:
            print(e)
            addErrorCoinsUserCoin(userUUID)
            raise HTTPException(status_code=500, detail=f"Failed to create thread: {e}")

        try:
            run = client.beta.threads.runs.create(
                thread_id=thread.id, assistant_id="asst_UnvTkcb3jaEMpEkdKehEhS5F"
            )
            # print(run)
            while run.status !="completed":
                run = client.beta.threads.runs.retrieve(
                    thread_id=thread.id, run_id=run.id
                )
                # print(run.status)

            rateLimitText = "Rate limit reached for gpt-4o in organization org-y5osZTkXKsWZIXJ61dasz87L"
            runString = str(run)
            result = rateLimitText in runString
            
            if result == False:
                print("Not Rate Limited")
            else:
                addRateLimitedTimes(userUUID)
                print("Rate Limited")

            messages = client.beta.threads.messages.list(thread_id=thread.id)
            print(messages.data[0].content[0].text.value)
            parsed_content = parse_value(f"{messages.data[0].content[0].text.value}")
            # print(parsed_content)
            addSuccessfullCoinsUserCoin(userUUID)
            return parsed_content
        except Exception as e:
            addErrorCoinsUserCoin(userUUID)
            print(e)
            raise HTTPException(status_code=500, detail=f"Failed to create and poll run: {e}")
    else:
        return False

@app.get("/echoInsightInternetCheck/")
async def internet_check():
    return json_version_Loaded

@app.get("/trackInfoDev/")
async def track_info(track_name: str, artist_name: str, spotifyClientID: str, spotifyClientSecret: str):
    client_id = spotifyClientID
    client_secret = spotifyClientSecret

    access_token = await get_access_token(client_id, client_secret)

    if access_token:
        track_data = await get_track_data(access_token, track_name, artist_name)
        if track_data and track_data['tracks']['items']:
            track = track_data['tracks']['items'][0]
            album_image_url = track['album']['images'][0]['url']
            track_name = track['name']
            first_artist_name = track['artists'][0]['name']
            varios_artistas = len(track['artists']) > 1
            track_url = track['external_urls']['spotify']

            result = [{
                "imagen": album_image_url,
                "Cancion": track_name,
                "Artista": first_artist_name,
                "VariosArtistas": varios_artistas,
                "URL": track_url
            }]
            return result
        else:
            raise HTTPException(status_code=404, detail="Track not found")
    else:
        raise HTTPException(status_code=500, detail="Failed to retrieve Spotify token")

@app.get("/echoInsightTrackInfoSpotify/")
async def track_info(track_name: str, artist_name: str, userUUID: str, musicService: str, playlistID: str):
    client_id = Spotify_Client_ID
    client_secret = Spotify_Client_Secret

    access_token = await get_access_token(client_id, client_secret)

    if access_token:
        track_data = await get_track_data(access_token, track_name, artist_name)
        if track_data and track_data['tracks']['items']:
            track = track_data['tracks']['items'][0]
            album_image_url = track['album']['images'][0]['url']
            track_name = track['name']
            first_artist_name = track['artists'][0]['name']
            varios_artistas = len(track['artists']) > 1
            track_url = track['external_urls']['spotify']

            result = [{
                "imagen": album_image_url,
                "Cancion": track_name,
                "Artista": first_artist_name,
                "VariosArtistas": varios_artistas,
                "URL": track_url
            }]
            addSongUser(userUUID=userUUID,imageURL=album_image_url,songName=track_name,artistName=first_artist_name,urlSong=track_url,musicService=musicService,playlistID=playlistID)
            return result
        else:
            raise HTTPException(status_code=404, detail="Track not found")
    else:
        raise HTTPException(status_code=500, detail="Failed to retrieve Spotify token")

@app.get("/echoInsightUserCheck/")
def get_database(userUUID: str):
    CONNECTION_STRING = mongodb_ConnectionString
    client = MongoClient(CONNECTION_STRING)
    dbname = client.EchoInsightDB
    EchoInsightDB = dbname["Users"]
    item_details = EchoInsightDB.find_one({"userUUID": f"{userUUID}"})
    if item_details:
        item_details_json = bson.json_util.dumps(item_details)
        item_details = json.loads(item_details_json)
    else:
        item_details = False
    return item_details

@app.get("/echoInsightCreateUser/")
def create_User(userUUID: str):

    try:
        CONNECTION_STRING = mongodb_ConnectionString
        client = MongoClient(CONNECTION_STRING)
        dbname = client.EchoInsightDB
        EchoInsightDB = dbname["Users"]

        item_details = EchoInsightDB.find_one({"userUUID": f"{userUUID}"})
        if item_details:
            return False
        else:
            utc_now = datetime.utcnow()
            central_time_offset = timedelta(hours=-6)
            current_datetime = utc_now + central_time_offset

            newUser = {
                "userUUID": userUUID,
                "coins": 0,
                "stats": {
                    "accountCreated": current_datetime,
                    "purchasedCoins": 0,
                    "reedemedCoins": 0,
                    "usedCoins": 0,
                    "refundedCoins": 0,
                    "successfullCoins": 0,
                    "errorCoins": 0,
                    "rateLimitedTimes": 0
                },
                "songs": []
            }

            insert_result = EchoInsightDB.insert_one(newUser)
            return insert_result.acknowledged
    except:
        return False

@app.get("/EchoInsightValidatePurchase/")
async def validate_purchase(transactionId: str, userUUID: str):
    urls = [
        f'https://api.storekit.itunes.apple.com/inApps/v1/transactions/{transactionId}',
        f'https://api.storekit-sandbox.itunes.apple.com/inApps/v1/transactions/{transactionId}'
    ]
    headers = {
        'Authorization': f'Bearer {generate_jwt_token()}'
    }

    async with httpx.AsyncClient() as client:
        for url in urls:
            response = await client.get(url, headers=headers)
            if response.status_code == 200:
                validation_data = response.json()
                validation_data = validation_data["signedTransactionInfo"]

                try:
                    CONNECTION_STRING = mongodb_ConnectionString
                    client = MongoClient(CONNECTION_STRING)
                    dbname = client.EchoInsightDB
                    EchoInsightDB = dbname["Purchases"]

                    item_details = EchoInsightDB.find_one({"receipt": f"{transactionId}"})
                    if item_details:
                        return "Receipt already redeemed"
                    else:
                        utc_now = datetime.utcnow()
                        central_time_offset = timedelta(hours=-6)
                        current_datetime = utc_now + central_time_offset

                        newPurchase = {
                            "receipt": transactionId,
                            "userUUID": userUUID,
                            "receiptCreated": current_datetime,
                            "refunded": False
                        }

                        insert_result = EchoInsightDB.insert_one(newPurchase)
                        
                        itemPurchased = decode_jwt(validation_data)
                        if itemPurchased == "AlfredTechDev.EchoInsight.4coins":
                            addUserCoins(userUUID, 4)
                            print("added 4 coins")
                        return insert_result.acknowledged
                except Exception as e:
                    return f"Could not check with Mongo: {str(e)}"

        raise HTTPException(status_code=500, detail="Failed to retrieve Apple Purchase from both production and sandbox")

#handle Refund
@app.get("/echoInsightRefundApple20242711Alfred/")
async def refund_request(request: Request):
    request_info = {
        "method": request.method,
        "url": str(request.url),
        "headers": dict(request.headers),
        "client": request.client.host,
        "json_version_loaded": json_version_Loaded
    }
    return request_info




if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000, timeout_keep_alive=360)
    